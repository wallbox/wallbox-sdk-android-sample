# README #

### What is this repository for? ###

This is a sample Android app to learn how to use WallboxSDK. In this sample project you will find how to use some functionalities in 3 different views:

### 1. Chargers list ###

This is the view you will find by default when you run the project on your device.

In this view, you can find an example of use for:

* [Login](https://wallbox.bitbucket.io/android/kotlindoc/com/wallbox/sdk/service/wallboxuserservice)
* Obtain the [list of chargers](https://wallbox.bitbucket.io/android/kotlindoc/com/wallbox/sdk/service/wallboxchargerservice) for the current user.
* Start/Stop scanning chargers [in reach](https://wallbox.bitbucket.io/android/kotlindoc/com/wallbox/sdk/service/wallboxchargerservice) of bluetooth.
* Check if a specific [charger](https://wallbox.bitbucket.io/android/kotlindoc/com/wallbox/sdk/charger/wallboxcharger) is currently in range of the bluetooth signal.

### 2. Add charger ###

This view is accessible from the plus button inside the chargers list menu option.

In this view, you can find an example of use for:

* [Link a charger](https://wallbox.bitbucket.io/android/kotlindoc/com/wallbox/sdk/service/wallboxchargerservice) to the current user.

### 3. Charger detail ###

To access this view, press one of the chargers listed in the chargers list.

In this view, you can find an example of use for:

* Obtain the instance of [WallboxCharger](https://wallbox.bitbucket.io/android/kotlindoc/com/wallbox/sdk/charger/wallboxcharger) needed to operate with the charger.
* [Connect/Disconnect](https://wallbox.bitbucket.io/android/kotlindoc/com/wallbox/sdk/charger/feature/connect/domain/connectfeature/) the charger
* [Pair](https://wallbox.bitbucket.io/android/kotlindoc/com/wallbox/sdk/charger/feature/pair/domain/pairfeature/) the charger
* [Subscribe/Unsubscribe](https://wallbox.bitbucket.io/android/kotlindoc/com/wallbox/sdk/charger/feature/status/domain/statusfeature/) to the changes of the charger Status
* Keep the current [status](https://wallbox.bitbucket.io/android/kotlindoc/com/wallbox/sdk/charger/feature/status/domain/statusfeature/) up to date in real time
* [Start/Stop](https://wallbox.bitbucket.io/android/kotlindoc/com/wallbox/sdk/charger/feature/status/domain/statusfeature/) status notifier
* Handling when a charger is connected or disconnected
* Recommended actions when going background and foreground
* [Start/stop](https://wallbox.bitbucket.io/android/kotlindoc/com/wallbox/sdk/charger/feature/charging/domain/chargingfeature/) charging
* [Lock/unlock](https://wallbox.bitbucket.io/android/kotlindoc/com/wallbox/sdk/charger/feature/lock/domain/lockfeature/) charger
* [Set/Edit name](https://wallbox.bitbucket.io/android/kotlindoc/com/wallbox/sdk/charger/feature/set_name/domain/setnamefeature) of charger
* [Unlink](https://wallbox.bitbucket.io/android/kotlindoc/com/wallbox/sdk/service/wallboxchargerservice/) charger from the current user


### How do I get set up? ###

* Add the *consumer.properties* file on the root folder with your credentials for accessing the SDK repository:
    
    `artifactoryExternalReleaseRepoUrl=`
    `artifactoryExternalReleaseConsumerUser=`
    `artifactoryExternalReleaseConsumerPassword=`
    
* Create a file named *token.properties* and put it inside te root folder. Inside this add a variable named *token* that contains your client token.
    
    `token="YOUR CLIENT TOKEN HERE"`
       
* You are ready to go!!
