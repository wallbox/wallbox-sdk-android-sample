package com.example.wallboxsdksample.model

import com.wallbox.sdk.model.WallboxBluetoothDevice
import com.wallbox.sdk.model.WallboxChargerInfo
import com.wallbox.sdk.model.WallboxChargerState
import com.wallbox.sdk.model.WallboxInternetConnectionType

class Charger(
    val serialNumber: Long,
    val name: String,
    val imageUrl: String?,
    var status: ChargerStatus? = ChargerStatus.NotAvailableConnection,
    val internetAvailability: Boolean,
    val internetConnectionType: WallboxInternetConnectionType?
)

fun List<Charger>.addBluetoothAvailability(wallboxDeviceBluetooth: WallboxBluetoothDevice) =
    find { it.serialNumber == wallboxDeviceBluetooth.id }
        .also {
            it?.status =
                ChargerStatus.build(
                    bluetoothAvailability = true,
                    internetAvailability = it?.internetAvailability ?: false
                )
        }

fun WallboxChargerInfo.toDomain() =
    Charger(
        serialNumber = id,
        name = name,
        imageUrl = imageUrl,
        status =
        ChargerStatus.build(
            bluetoothAvailability = false,
            internetAvailability = state != WallboxChargerState.UNAVAILABLE
        ),
        internetAvailability = state != WallboxChargerState.UNAVAILABLE,
        internetConnectionType = internetConnectionType
    )