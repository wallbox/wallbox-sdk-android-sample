package com.example.wallboxsdksample.model

sealed class ChargerStatus {

    companion object {
        fun build(bluetoothAvailability: Boolean, internetAvailability: Boolean) =
            when {
                bluetoothAvailability && internetAvailability -> BluetoothAndInternetConnection
                bluetoothAvailability && !internetAvailability -> BluetoothConnection
                !bluetoothAvailability && internetAvailability -> InternetConnection
                else -> NotAvailableConnection
            }
    }

    object BluetoothAndInternetConnection: ChargerStatus()
    object BluetoothConnection: ChargerStatus()
    object InternetConnection: ChargerStatus()
    object NotAvailableConnection: ChargerStatus()
}