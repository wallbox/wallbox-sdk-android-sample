package com.example.wallboxsdksample.model

data class ChargerIdentification(
    val pin: Long,
    val uid: String,
    val countryCode: String,
    val stateCode: String?
) {
    companion object {
        fun empty() = ChargerIdentification(pin = 0L, uid = "", countryCode = "", stateCode = null)
    }
}