package com.example.wallboxsdksample.addCharger

import com.example.wallboxsdksample.model.ChargerIdentification
import com.wallbox.sdk.WallboxSdk
import com.wallbox.sdk.internal.common.entity.WallboxApiError
import com.wallbox.sdk.model.WallboxCountry
import com.wallbox.wallboxutils.model.WallboxError
import com.wallbox.wallboxutils.model.fold
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext
import kotlin.properties.Delegates

class AddChargerPresenter(
        private var view: AddChargerView?
) : CoroutineScope {

    private var currentJob: Job? = null

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + Job()

    private val chargerService by lazy { WallboxSdk.serviceSupplier.chargerService }

    private var chargerIdentification: ChargerIdentification
            by Delegates.observable(ChargerIdentification.empty()) { _, _, _ ->
                checkLinkCharger()
            }

    fun linkCharger() {
        view?.showLoading()
        currentJob = launch {
            withContext(Dispatchers.IO) {
                // Link the ownership of the charger to the user
                // See documentation here: https://wallbox.bitbucket.io/android/kotlindoc/com/wallbox/sdk/service/wallboxchargerservice
                chargerService.linkCharger(
                    chargerSerialNumber = chargerIdentification.pin,
                    chargerPuk = chargerIdentification.uid,
                    countryCode = chargerIdentification.countryCode,
                    stateCode = chargerIdentification.stateCode,
                    onChargerLinked = { wallboxResult ->
                        wallboxResult.fold(
                            left = {
                                // See documentation here: https://wallbox.bitbucket.io/android/kotlindoc/com/wallbox/sdk/internal/common/entity/wallboxapierror
                                view?.hideLoading()
                                view?.showError(it.msg)
                            },
                            right = {
                                view?.hideLoading()
                                view?.successfulChargerAdded()
                            }
                        )
                    }
                )
            }
        }
    }

    fun onDestroy() {
        currentJob?.cancel()
        view = null
    }

    fun onPinChanged(pin: String) {
        chargerIdentification = chargerIdentification.copy(pin = pin.toLong())
    }

    fun onUidChanged(uid: String) {
        chargerIdentification = chargerIdentification.copy(uid = uid)
    }

    fun onCountryCodeChanged(countryCode: String, position: Int) {
        chargerIdentification = if (position == 0) {
            chargerIdentification.copy(countryCode = "")
        } else {
            chargerIdentification.copy(countryCode = countryCode)
        }

        checkCountryCode()
    }

    fun onStateCodeChanged(stateCode: String, position: Int) {
        chargerIdentification = if (position == 0) {
            chargerIdentification.copy(stateCode = null)
        } else {
            chargerIdentification.copy(stateCode = stateCode)
        }
    }

    private fun checkLinkCharger() {
        if (chargerIdentification.pin == 0L
                || chargerIdentification.uid.isEmpty()
                || chargerIdentification.countryCode.isEmpty()) {
            view?.setButtonDisable()
        } else {
            view?.setButtonEnable()
        }
    }

    private fun checkCountryCode() {
        if (chargerIdentification.countryCode != ESP_COUNTRY_CODE) {
            view?.hideStateCode()
            chargerIdentification = chargerIdentification.copy(stateCode = null)
        } else {
            view?.showStateCode()
        }
    }

    companion object {
        private const val ESP_COUNTRY_CODE = "ESP"
    }
}