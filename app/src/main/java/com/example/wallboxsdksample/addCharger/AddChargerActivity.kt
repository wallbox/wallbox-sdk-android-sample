package com.example.wallboxsdksample.addCharger

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import com.example.wallboxsdksample.R
import com.example.wallboxsdksample.UIComponent.showSnackBarError
import com.example.wallboxsdksample.databinding.ActivityAddChargerBinding
import com.example.wallboxsdksample.hideKeyboard
import com.example.wallboxsdksample.onItemSelected
import com.example.wallboxsdksample.onTextChanged
import kotlinx.android.synthetic.main.activity_add_charger.*


class AddChargerActivity : AppCompatActivity(), AddChargerView {

    companion object {
        fun create(context: Context) = Intent(context, AddChargerActivity::class.java)
    }

    private val presenter: AddChargerPresenter by lazy {
        AddChargerPresenter(this)
    }

    private val binding: ActivityAddChargerBinding by lazy {
        ActivityAddChargerBinding.inflate(layoutInflater)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        setUpActionBar()
        setUpListeners()
        setUpSpinners()
    }

    override fun onDestroy() {
        presenter.onDestroy()
        super.onDestroy()
    }

    override fun showError(message: String) {
        binding.cdlRoot.showSnackBarError(message)
    }

    override fun setButtonEnable() {
        btnAdd.isEnabled = true
    }

    override fun setButtonDisable() {
        btnAdd.isEnabled = false
    }

    override fun successfulChargerAdded() {
        setResult(Activity.RESULT_OK)
        finish()
    }

    override fun showLoading() {
        binding.progressBar.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        binding.progressBar.visibility = View.GONE
    }

    override fun showStateCode() {
        binding.spnState.visibility = View.VISIBLE
    }

    override fun hideStateCode() {
        binding.spnState.visibility = View.GONE
    }

    override fun onBackPressed() {
        super.onBackPressed()
        setResult(Activity.RESULT_CANCELED)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home)
            onBackPressed()
        return super.onOptionsItemSelected(item)
    }

    private fun setUpListeners() {
        edtPin.onTextChanged {
            presenter.onPinChanged(it)
        }

        edtUid.onTextChanged {
            presenter.onUidChanged(it)
        }

        btnAdd.setOnClickListener {
            hideKeyboard()
            presenter.linkCharger()
        }

        binding.spnCountry.onItemSelected { _, _, position, _ ->
            presenter.onCountryCodeChanged(
                resources.getStringArray(R.array.country_code_array)[position],
                position
            )
        }

        binding.spnState.onItemSelected { _, _, position, _ ->
            presenter.onStateCodeChanged(
                resources.getStringArray(R.array.state_code_array)[position],
                position
            )
        }
    }

    private fun setUpSpinners() {
        ArrayAdapter.createFromResource(
            this,
            R.array.country_code_array,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            binding.spnCountry.adapter = adapter
        }

        ArrayAdapter.createFromResource(
            this,
            R.array.state_code_array,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            binding.spnState.adapter = adapter
        }
    }

    private fun setUpActionBar() {
        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setDisplayShowHomeEnabled(true)
            title = resources.getString(R.string.add_charger)
        }
    }
}