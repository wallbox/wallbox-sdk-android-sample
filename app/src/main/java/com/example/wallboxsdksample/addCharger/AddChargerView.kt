package com.example.wallboxsdksample.addCharger

interface AddChargerView {

    fun showError(message: String)

    fun setButtonEnable()

    fun setButtonDisable()

    fun successfulChargerAdded()

    fun showLoading()

    fun hideLoading()

    fun showStateCode()

    fun hideStateCode()

}