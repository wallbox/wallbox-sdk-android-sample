package com.example.wallboxsdksample.chargerDetail.model

import com.wallbox.sdk.charger.WallboxCharger
import com.wallbox.sdk.model.WallboxChargerState
import com.wallbox.sdk.model.WallboxChargerStatus
import com.wallbox.sdk.model.WallboxConnection

typealias ChargerVersion = Pair<ChargerDetailUIModel.VersionTypeUIModel, String>

class ChargerDetailUIModel(private val charger: WallboxCharger?) {

    sealed class ChargerConnectionStatusUIModel {
        object Connected: ChargerConnectionStatusUIModel()
        object Connecting: ChargerConnectionStatusUIModel()
        object Disconnected: ChargerConnectionStatusUIModel()
    }

    sealed class VersionTypeUIModel {
        object Software: VersionTypeUIModel()
        object Firmware: VersionTypeUIModel()
    }

    sealed class ConnectionTypeUIModel {
        object Bluetooth: ConnectionTypeUIModel()
        object Cloud: ConnectionTypeUIModel()
    }

    sealed class ConnectionStateUIModel {
        object Connected: ConnectionStateUIModel()
        object Unavailable: ConnectionStateUIModel()
        object Charging: ConnectionStateUIModel()
        object Discharging: ConnectionStateUIModel()
        object WaitingDemand: ConnectionStateUIModel()
        object Scheduled: ConnectionStateUIModel()
        object Paused: ConnectionStateUIModel()
        object Error: ConnectionStateUIModel()
    }

    data class LockStatus(val isLocked: Boolean)

    data class ChargeActionStateUIModel(val isCharging: Boolean)

    private var status: WallboxChargerStatus? = null

    fun updateStatus(status: WallboxChargerStatus?) {
        this.status = status
    }

    val connectionStatus: ChargerConnectionStatusUIModel
        get() = when {
            charger == null ->  ChargerConnectionStatusUIModel.Connecting
            charger.isConnected -> ChargerConnectionStatusUIModel.Connected
            else -> ChargerConnectionStatusUIModel.Disconnected
        }

    val chargerName: String
        get() = charger?.name ?: ""


    val version: ChargerVersion?
        get() =
            charger?.let {
                if (it.hasSoftware) {
                    ChargerVersion(VersionTypeUIModel.Software, it.versions.softwareVersion)
                } else {
                    ChargerVersion(VersionTypeUIModel.Firmware, "${it.versions.firmwareVersion}")
                }
            }

    val connectionType: ConnectionTypeUIModel?
        get() = when (charger?.connectedBy) {
            WallboxConnection.BLUETOOTH -> ConnectionTypeUIModel.Bluetooth
            WallboxConnection.CLOUD -> ConnectionTypeUIModel.Cloud
            else -> null
        }

    val connectionState: ConnectionStateUIModel?
        get() = when (status?.state) {
            WallboxChargerState.AVAILABLE,
            WallboxChargerState.CONNECTED_MID_SAFETY_MARGIN_EXCEEDED,
            WallboxChargerState.CONNECTED_WAITING_ADMIN_AUTH_FOR_MID,
            WallboxChargerState.POWER_SHARING_WAITING_CURRENT_ASSIGNATION,
            WallboxChargerState.CONNECTED_IN_QUEUE_POWER_BOOST,
            WallboxChargerState.END_SCHEDULE -> ConnectionStateUIModel.Connected
            WallboxChargerState.UNAVAILABLE -> ConnectionStateUIModel.Unavailable
            WallboxChargerState.CHARGING, WallboxChargerState.OCPP_CHARGE_FINISHING -> ConnectionStateUIModel.Charging
            WallboxChargerState.DISCHARGING -> ConnectionStateUIModel.Discharging
            WallboxChargerState.CONNECTED_CAR_DEMAND -> ConnectionStateUIModel.WaitingDemand
            WallboxChargerState.WAITING_NEXT_SCHEDULE -> ConnectionStateUIModel.Scheduled
            WallboxChargerState.PAUSED -> ConnectionStateUIModel.Paused
            WallboxChargerState.ERROR, WallboxChargerState.POWER_SHARING_UNCONFIGURED -> ConnectionStateUIModel.Error
            else -> null
        }

    val lockStatus: LockStatus?
        get() = status?.let { LockStatus(it.isLocked) }

    val chargeActionState: ChargeActionStateUIModel?
        get() {
            val isChargingFeatureAvailable = charger?.isChargingFeatureAvailable()?.isSuccess == true
            return ChargeActionStateUIModel(isChargingFeatureAvailable && status?.state == WallboxChargerState.CHARGING)
        }

}
