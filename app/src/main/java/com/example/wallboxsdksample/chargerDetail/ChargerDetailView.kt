package com.example.wallboxsdksample.chargerDetail

import com.example.wallboxsdksample.chargerDetail.model.ChargerDetailUIModel
import com.example.wallboxsdksample.chargerDetail.model.ChargerVersion

interface ChargerDetailView {

    sealed class Action {
        object Charge: Action()
        object Lock: Action()
        object EditNameClick: Action()
        data class EditNameSubmit(val name: String): Action()
        object UnlinkCharger: Action()
    }

    fun setChargerName(chargerName: String, connectionStatus: ChargerDetailUIModel.ChargerConnectionStatusUIModel)

    fun setConnectionType(connectionType: ChargerDetailUIModel.ConnectionTypeUIModel?)

    fun setVersion(version: ChargerVersion?)

    fun setChargingState(state: ChargerDetailUIModel.ConnectionStateUIModel?)

    fun setLockStatus(lockStatus: ChargerDetailUIModel.LockStatus?)

    fun setChargeAction(chargingState: ChargerDetailUIModel.ChargeActionStateUIModel?)

    fun setLockAction(lockStatus: ChargerDetailUIModel.LockStatus?)

    fun openEditNameDialog()

    fun showError(message: String?)

    fun notifyChargeConfigChanged()

    fun close()
}