package com.example.wallboxsdksample.chargerDetail

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import com.example.wallboxsdksample.R
import com.example.wallboxsdksample.UIComponent.showSnackBarError
import com.example.wallboxsdksample.chargerDetail.model.ChargerDetailUIModel
import com.example.wallboxsdksample.chargerDetail.model.ChargerVersion
import com.example.wallboxsdksample.databinding.ActivtyChargerDetailBinding

class ChargerDetailActivity : AppCompatActivity(), ChargerDetailView {

    private val presenter: ChargerDetailPresenter by lazy {
        ChargerDetailPresenter(this)
    }

    private val binding: ActivtyChargerDetailBinding by lazy {
        ActivtyChargerDetailBinding.inflate(layoutInflater)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        setUpActionBar()
        presenter.onViewCreated(intent?.extras?.getLong(ID_CHARGER) ?: 0L)
        setupListeners()
    }

    override fun onDestroy() {
        presenter.onDestroy()
        super.onDestroy()
    }

    private fun setupListeners() {
        binding.apply {
            icdChargeAction.setOnClickListener {
                presenter.onActionClicked(ChargerDetailView.Action.Charge)
            }
            icdLockAction.setOnClickListener {
                presenter.onActionClicked(ChargerDetailView.Action.Lock)
            }
            icdEditName.setOnClickListener {
                presenter.onActionClicked(ChargerDetailView.Action.EditNameClick)
            }
            icdUnlink.setOnClickListener {
                presenter.onActionClicked(ChargerDetailView.Action.UnlinkCharger)
            }
        }
    }

    override fun setChargerName(name: String, status: ChargerDetailUIModel.ChargerConnectionStatusUIModel) {
        supportActionBar?.title = name
        supportActionBar?.subtitle = when (status) {
            ChargerDetailUIModel.ChargerConnectionStatusUIModel.Connected -> getString(R.string.connected_status_connected)
            ChargerDetailUIModel.ChargerConnectionStatusUIModel.Connecting -> getString(R.string.connected_status_connecting)
            ChargerDetailUIModel.ChargerConnectionStatusUIModel.Disconnected -> getString(R.string.connected_status_disconnected)
        }
    }

    override fun setConnectionType(connectionTypeUIModel: ChargerDetailUIModel.ConnectionTypeUIModel?) {
        binding.icdConnectionInfo.detail = when (connectionTypeUIModel) {
            is ChargerDetailUIModel.ConnectionTypeUIModel.Bluetooth -> getString(R.string.detail_info_connected_by_bluetooth)
            is ChargerDetailUIModel.ConnectionTypeUIModel.Cloud -> getString(R.string.detail_info_connected_by_cloud)
            else -> getString(R.string.detail_info_connected_by_none)
        }
    }

    override fun setVersion(version: ChargerVersion?) {
        version?.let {
            binding.icdVersionInfo.title = when (it.first) {
                is ChargerDetailUIModel.VersionTypeUIModel.Software -> getString(R.string.title_info_sw_version)
                is ChargerDetailUIModel.VersionTypeUIModel.Firmware -> getString(R.string.title_info_fw_version)
            }
            binding.icdVersionInfo.detail = it.second
        }
    }

    override fun setChargingState(state: ChargerDetailUIModel.ConnectionStateUIModel?) {
        binding.icdChargingStateInfo.detail = when (state) {
            is ChargerDetailUIModel.ConnectionStateUIModel.Connected -> getString(R.string.detail_info_charging_state_connected)
            is ChargerDetailUIModel.ConnectionStateUIModel.Unavailable -> getString(R.string.detail_info_charging_state_unavailable)
            is ChargerDetailUIModel.ConnectionStateUIModel.Charging -> getString(R.string.detail_info_charging_state_charging)
            is ChargerDetailUIModel.ConnectionStateUIModel.Discharging -> getString(R.string.detail_info_charging_state_discharging)
            is ChargerDetailUIModel.ConnectionStateUIModel.WaitingDemand -> getString(R.string.detail_info_charging_state_connected_car_demand)
            is ChargerDetailUIModel.ConnectionStateUIModel.Scheduled -> getString(R.string.detail_info_charging_state_scheduled)
            is ChargerDetailUIModel.ConnectionStateUIModel.Paused -> getString(R.string.detail_info_charging_state_paused)
            is ChargerDetailUIModel.ConnectionStateUIModel.Error -> getString(R.string.detail_info_charging_state_error)
            else -> getString(R.string.detail_info_charging_state_none)
        }
    }

    override fun setLockStatus(lockStatus: ChargerDetailUIModel.LockStatus?) {
        binding.icdLockStatusInfo.detail = lockStatus?.let {
            if (it.isLocked) {
                getString(R.string.detail_info_lock_status_locked)
            } else {
                getString(R.string.detail_info_lock_status_unlocked)
            }
        } ?: getString(R.string.detail_info_lock_status_none)
    }

    override fun setChargeAction(chargingState: ChargerDetailUIModel.ChargeActionStateUIModel?) {
        chargingState?.let {
            binding.icdChargeAction.title = if (it.isCharging) {
                getString(R.string.title_action_charge_action_pause)
            } else {
                getString(R.string.title_action_charge_action_play)
            }
            binding.icdChargeAction.visibility = View.VISIBLE
        } ?: run {
            binding.icdChargeAction.visibility = View.GONE
        }
    }

    override fun setLockAction(lockStatus: ChargerDetailUIModel.LockStatus?) {
        lockStatus?.let {
            binding.icdLockAction.title = if (it.isLocked) {
                getString(R.string.title_action_unlock)
            } else {
                getString(R.string.title_action_lock)
            }
            binding.icdLockAction.visibility = View.VISIBLE
        } ?: run {
            binding.icdLockAction.visibility = View.GONE
        }
    }

    override fun openEditNameDialog() {
        val builder = AlertDialog.Builder(this@ChargerDetailActivity)
        val dialogLayout = layoutInflater.inflate(R.layout.dialog_edit_charger_name, null)
        val editText = dialogLayout.findViewById<EditText>(R.id.edtChargerName)
        builder.setTitle(getString(R.string.dialog_edit_name_title))
        builder.setMessage(getString(R.string.dialog_edit_name_message))
        builder.setView(dialogLayout)
        builder.setPositiveButton(R.string.dialog_edit_name_positive_action) { _, _ ->
            presenter.onActionClicked(ChargerDetailView.Action.EditNameSubmit(editText.text.toString()))
        }
        builder.show()
    }

    override fun showError(message: String?) {
        message?.let {
            binding.cdlRoot.showSnackBarError(message)
        } ?: run {
            binding.cdlRoot.showSnackBarError(getString(R.string.error_api))
        }
    }

    override fun notifyChargeConfigChanged() {
        setResult(Activity.RESULT_OK)
    }

    override fun close() {
        finish()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home)
            onBackPressed()
        return super.onOptionsItemSelected(item)
    }

    private fun setUpActionBar() {
        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setDisplayShowHomeEnabled(true)
        }
    }

    companion object {
        private const val ID_CHARGER = "id"

        fun create(context: Context, serialNumber: Long) =
                Intent(context, ChargerDetailActivity::class.java).apply {
                    putExtra(ID_CHARGER, serialNumber)
                }
    }
}