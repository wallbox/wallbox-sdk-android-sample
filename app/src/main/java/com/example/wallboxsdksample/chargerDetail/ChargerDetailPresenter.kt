package com.example.wallboxsdksample.chargerDetail

import com.example.wallboxsdksample.chargerDetail.ChargerDetailView.Action.*
import com.example.wallboxsdksample.chargerDetail.model.ChargerDetailUIModel
import com.wallbox.sdk.WallboxSdk
import com.wallbox.sdk.charger.WallboxCharger
import com.wallbox.sdk.model.WallboxChargerState.*
import com.wallbox.wallboxutils.model.WallboxError
import com.wallbox.wallboxutils.model.fold
import com.wallbox.wallboxutils.model.foldFailure
import com.wallbox.wallboxutils.model.foldSuccess
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext


class ChargerDetailPresenter(private var view: ChargerDetailView?) : CoroutineScope {

    private var currentJob: Job? = null

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + Job()

    private val chargerService by lazy { WallboxSdk.serviceSupplier.chargerService }
    private lateinit var chargerDetailUIModel: ChargerDetailUIModel

    private var charger: WallboxCharger? = null
        set(value) {
            chargerDetailUIModel = ChargerDetailUIModel(value)
            field = value
        }

    fun onViewCreated(serialNumber: Long) {
        currentJob = launch {
            withContext(Dispatchers.IO) {
                setupCharger(serialNumber)
            }
        }
    }

    /**
     * 1. Obtain the instance of WallboxCharger needed to operate with the charger.
     * We need this object to use all the features enumerated in the documentation.
     * If charger comes as null that means the serial number is not found.
     * See documentation here: https://wallbox.bitbucket.io/android/kotlindoc/com/wallbox/sdk/charger/wallboxcharger
     */
    private suspend fun setupCharger(serialNumber: Long) {
        charger = chargerService.createChargerBy(serialNumber)
        connectCharger()
        withContext(Dispatchers.Main) {
            reloadData()
        }
    }

    /**
     * 2. Connect the charger
     * See documentation here: https://wallbox.bitbucket.io/android/kotlindoc/com/wallbox/sdk/charger/feature/connect/domain/connectfeature
     */
    private fun connectCharger() {
        charger?.connect(
                onDisconnect = {
                    charger?.stopStatusNotifier()
                },
                onConnect = { result ->
                    result.fold(
                            left = {
                                showError(it)
                            },
                            right = {
                                pairCharger()
                            }
                    )
                }
        ) ?: showError()
    }

    /**
     * 3. Pair the charger
     * See documentation here: https://wallbox.bitbucket.io/android/kotlindoc/com/wallbox/sdk/charger/feature/pair/domain/pairfeature
     */
    private fun pairCharger() {
        charger?.pair(
                onSuggestUpdate = {},
                onSessionsSyncing = {},
                onPaired = { result ->
                    result.fold(
                            left = {
                                showError(it)
                            },
                            right = {
                                reloadData()
                                subscribeForStatus()
                            }
                    )
                }
        )
    }

    /**
     * 4. Subscribe to the changes of the charger Status and keep it up to date
     * See documentation here: https://wallbox.bitbucket.io/android/kotlindoc/com/wallbox/sdk/charger/feature/status/domain/statusfeature
     */
    private fun subscribeForStatus() {
        // Start receiving the changes of the charger Status
        charger?.startStatusNotifier { result ->
            if (result.isSuccess) {
                chargerDetailUIModel.updateStatus(result.foldSuccess())
                reloadData()
            }
        } ?: showError()
    }

    /**
     * For WallboxError details see documentation here: https://wallbox.bitbucket.io/android/kotlindoc/com/wallbox/sdk/internal/common/entity/wallboxapierror
     */
    private fun showError(error: WallboxError? = null, message: String? = null) {
        view?.showError(error?.message ?: message)
    }

    private fun reloadData() {
        view?.setChargerName(chargerDetailUIModel.chargerName, chargerDetailUIModel.connectionStatus)
        view?.setVersion(chargerDetailUIModel.version)
        view?.setConnectionType(chargerDetailUIModel.connectionType)
        view?.setChargingState(chargerDetailUIModel.connectionState)
        view?.setLockStatus(chargerDetailUIModel.lockStatus)
        view?.setChargeAction(chargerDetailUIModel.chargeActionState)
        view?.setLockAction(chargerDetailUIModel.lockStatus)
    }

    fun onActionClicked(action: ChargerDetailView.Action) {
        when (action) {
            is Charge -> chargeAction()
            is Lock -> lockAction()
            is EditNameClick -> openEditNameDialog()
            is EditNameSubmit -> editNameAction(action.name)
            is UnlinkCharger -> unlinkChargerAction()
        }
    }

    private fun chargeAction() {
        // Check if charging feature is available for the current charger
        if (charger?.isChargingFeatureAvailable()?.isSuccess == true) {
            // Get the last known status of the charger
            when (charger?.lastKnownStatus?.state) {
                CHARGING -> stopCharging()
                else -> startCharging()
            }
        } else {
            showError(message = "Charging feature not available")
        }
    }

    /**
     * Start charging with the current charger
     * See documentation here: https://wallbox.bitbucket.io/android/kotlindoc/com/wallbox/sdk/charger/feature/charging/domain/chargingfeature/
     */
    private fun startCharging() {
        charger?.startCharging { result ->
            if (result.isFailure) {
                showError(result.foldFailure())
            }
        } ?: showError()
    }

    /**
     * Stop charging with the current charger
     */
    private fun stopCharging() {
        charger?.stopCharging { result ->
            if (result.isFailure) {
                showError(result.foldFailure())
            }
        } ?: showError()
    }

    private fun lockAction() {
        charger?.apply {
            if (isLockFeatureAvailable().isSuccess) {
                if (lastKnownStatus?.isLocked == true) {
                    unlock()
                } else {
                    lock()
                }
            } else {
                showError(message = "Lock feature not available")
            }
        }
    }

    /**
     *  Lock the charger
     *  See documentation here: https://wallbox.bitbucket.io/android/kotlindoc/com/wallbox/sdk/charger/feature/lock/domain/lockfeature
     */
    private fun lock() {
        charger?.lock { result ->
            if (result.isFailure) {
                showError(result.foldFailure())
            }
        } ?: showError()
    }

    /**
     *  Unlock the charger
     *  See documentation here: https://wallbox.bitbucket.io/android/kotlindoc/com/wallbox/sdk/charger/feature/lock/domain/lockfeature
     */
    private fun unlock() {
        charger?.unlock { result ->
            if (result.isFailure) {
                showError(result.foldFailure())
            }
        } ?: showError()
    }

    private fun openEditNameDialog() {
        view?.openEditNameDialog()
    }

    private fun editNameAction(name: String) {
        charger?.let { charger ->
            if (charger.isSetNameFeatureAvailable().isSuccess) {
                setName(name)
            } else {
                showError(message = "Set name feature not available")
            }
        } ?: showError()
    }

    /**
     * Set or edit the name of the charger
     * See documentation here: https://wallbox.bitbucket.io/android/kotlindoc/com/wallbox/sdk/charger/feature/set_name/domain/setnamefeature
     */
    private fun setName(name: String) {
        if (name.isEmpty()) {
            showError(message = "Type a valid name")
        } else {
            charger?.setName(name) {
                if (it.isFailure) {
                    showError(it.foldFailure())
                } else {
                    reloadData()
                    view?.notifyChargeConfigChanged()
                }
            } ?: showError()
        }
    }

    /**
     * Unlink charger from the current user
     * See documentation here: https://wallbox.bitbucket.io/android/kotlindoc/com/wallbox/sdk/service/wallboxchargerservice
     */
    private fun unlinkChargerAction() {
        charger?.id?.let { serialNumber ->
            chargerService.unlinkCharger(serialNumber) { result ->
                result.fold(
                        left = {
                            showError(it)
                        },
                        right = {
                            view?.notifyChargeConfigChanged()
                            view?.close()
                        }
                )
            }
        } ?: showError()
    }

    fun onDestroy() {
        currentJob?.cancel()
        // Don't forget to unsubscribe when the subscribed target (in this case self) is deallocated
        charger?.stopStatusNotifier()
        view = null
    }
}