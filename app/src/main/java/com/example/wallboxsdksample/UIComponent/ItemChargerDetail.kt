package com.example.wallboxsdksample.UIComponent

import android.content.Context
import android.os.Build
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import com.example.wallboxsdksample.R
import com.example.wallboxsdksample.databinding.ItemChargerDetailBinding

class ItemChargerDetail @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    private val binding: ItemChargerDetailBinding =
        ItemChargerDetailBinding.inflate(LayoutInflater.from(context), this, true)

    var title: String? = null
        set(value) {
            binding.lblTitleInfo.text = value
            field = value
        }

    var detail: String? = null
        set(value) {
            binding.lblSubtitleInfo.text = value
            field = value
        }

    var color: Int = R.color.black
        set(value) {
            binding.lblTitleInfo.setTextColor(value)
        }

    init {
        val attributes = context.obtainStyledAttributes(
            attrs,
            R.styleable.ItemChargerDetail
        )
        title = attributes.getString(R.styleable.ItemChargerDetail_title)
        detail = attributes.getString(R.styleable.ItemChargerDetail_detail)
        color = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            attributes.getColor(R.styleable.ItemChargerDetail_color, resources.getColor(R.color.textColorPrimary, context.theme))
        } else {
            attributes.getColor(R.styleable.ItemChargerDetail_color, resources.getColor(R.color.textColorPrimary))
        }
        attributes.recycle()
    }
}