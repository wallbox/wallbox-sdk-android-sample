package com.example.wallboxsdksample.chargerList

import com.example.wallboxsdksample.model.Charger
import com.wallbox.sdk.model.WallboxChargerInfo

interface ChargerListView {

    fun showError(message: String, call: () -> Unit)

    fun loadChargers(chargersLinked: List<Charger>)

    fun showLoading()

    fun hideLoading()

    fun checkPermissions()

    fun showAddedChargerSuccessfully()

    fun cleanAdapter()

    fun checkBluetoothIsActive()
}