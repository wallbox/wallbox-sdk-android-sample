package com.example.wallboxsdksample.chargerList

import com.example.wallboxsdksample.BuildConfig
import com.example.wallboxsdksample.model.Charger
import com.example.wallboxsdksample.model.addBluetoothAvailability
import com.example.wallboxsdksample.model.toDomain
import com.wallbox.sdk.WallboxSdk
import com.wallbox.sdk.model.WallboxChargerInfo
import com.wallbox.wallboxutils.model.WallboxError
import com.wallbox.wallboxutils.model.fold
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

class ChargerListPresenter(private var view: ChargerListView?) : CoroutineScope {

    private var currentJob: Job? = null

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + Job()

    private val userService by lazy { WallboxSdk.serviceSupplier.userService }
    private val chargerService by lazy { WallboxSdk.serviceSupplier.chargerService }

    private val chargersInfo: MutableList<Charger> = mutableListOf()

    fun login() {
        view?.showLoading()
        currentJob = launch {
            withContext(Dispatchers.IO) {
                // Login with WallboxUserService
                // See documentation here: https://wallbox.bitbucket.io/android/kotlindoc/com/wallbox/sdk/service/wallboxuserservice
                userService.login(BuildConfig.TOKEN) { loginResult ->
                    loginResult.fold(
                        left = { manageError(it) { login() } },
                        right = { fetchLinkedChargers() }
                    )
                }
            }
        }
    }

    fun onDestroy() {
        currentJob?.cancel()
        view = null
    }

    fun scanChargers() {
        currentJob = launch {
            withContext(Dispatchers.IO) {
                // Scan chargers in reach of bluetooth
                // See documentation here: https://wallbox.bitbucket.io/android/kotlindoc/com/wallbox/sdk/service/wallboxchargerservice
                chargerService.scanChargers(
                    onDeviceFound = { chargersInfo.addBluetoothAvailability(it) },
                    onScanFinished = {
                        it.forEach { wallboxDeviceBluetooth ->
                            chargersInfo.addBluetoothAvailability(wallboxDeviceBluetooth)
                        }
                        loadChargers()
                    }
                )
            }
        }
    }

    fun loadChargers() {
        view?.apply {
            hideLoading()
            loadChargers(chargersInfo)
        }
    }

    fun reloadLinkedChargers() {
        view?.cleanAdapter()
        chargersInfo.clear()
        fetchLinkedChargers()
    }

    fun onChargerAddedSuccessfully() {
        view?.showAddedChargerSuccessfully()
    }

    fun onPermissionsGranted() {
        view?.checkBluetoothIsActive()
    }

    private fun fetchLinkedChargers() {
        view?.showLoading()
        currentJob = launch {
            withContext(Dispatchers.IO) {
                // Obtain the list of chargers for the current user
                // See documentation here: https://wallbox.bitbucket.io/android/kotlindoc/com/wallbox/sdk/service/wallboxchargerservice
                chargerService.getChargersInfo { chargersResult ->
                    chargersResult.fold(
                        left = { manageError(it) { fetchLinkedChargers() } },
                        right = { manageChargerListSuccess(it) }
                    )
                }
            }
        }
    }

    private fun manageChargerListSuccess(wallboxChargersInfo: List<WallboxChargerInfo>) {
        wallboxChargersInfo.forEach { wallboxChargerInfo ->
            chargersInfo.add(wallboxChargerInfo.toDomain())
        }

        view?.checkPermissions()
    }

    private fun manageError(error: WallboxError, reCall: () -> Unit) {
        view?.apply {
            hideLoading()
            showError(error.msg) { reCall() }
        }
    }
}