package com.example.wallboxsdksample.chargerList

import android.Manifest
import android.app.Activity
import android.app.Instrumentation
import android.bluetooth.BluetoothAdapter
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.wallboxsdksample.*
import com.example.wallboxsdksample.UIComponent.showSnackBarError
import com.example.wallboxsdksample.UIComponent.showSnackBarSuccessful
import com.example.wallboxsdksample.addCharger.AddChargerActivity
import com.example.wallboxsdksample.chargerDetail.ChargerDetailActivity
import com.example.wallboxsdksample.databinding.ActivityChargerListBinding
import com.example.wallboxsdksample.model.Charger
import kotlinx.android.synthetic.main.activity_charger_list.view.*

class ChargerListActivity : AppCompatActivity(), ChargerListView {

    companion object {
        private const val REQUEST_ENABLE_BT = 100
        private const val REQUEST_ADD_CHARGER = 101
        private const val REQUEST_LOCATION = 102
        private const val REQUEST_CONFIG_CHARGER = 103
    }

    private val presenter: ChargerListPresenter by lazy {
        ChargerListPresenter(this)
    }

    private val binding: ActivityChargerListBinding by lazy {
        ActivityChargerListBinding.inflate(layoutInflater)
    }

    private val bluetoothAdapter: BluetoothAdapter by lazy {
        BluetoothAdapter.getDefaultAdapter()
    }

    private val chargerListAdapter: ChargerListAdapter by lazy {
        ChargerListAdapter { navigateToChargerDetail(serialNumber = it) }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        setUpRecyclerView()
        setUpListeners()
        presenter.login()
    }

    override fun onDestroy() {
        presenter.onDestroy()
        super.onDestroy()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when {
            resultCode == Activity.RESULT_OK && requestCode == REQUEST_ENABLE_BT ->
                presenter.scanChargers()
            resultCode == Activity.RESULT_OK && requestCode == REQUEST_ADD_CHARGER -> {
                presenter.onChargerAddedSuccessfully()
                presenter.reloadLinkedChargers()
            }
            resultCode == Activity.RESULT_OK && requestCode == REQUEST_CONFIG_CHARGER -> {
                presenter.reloadLinkedChargers()
            }
            resultCode == Activity.RESULT_CANCELED && requestCode == REQUEST_ENABLE_BT ->
                presenter.loadChargers()
        }
    }

    override fun showError(message: String, call: () -> Unit) {
        binding.cdlRoot.showSnackBarError(message, call)
    }

    override fun loadChargers(chargersLinked: List<Charger>) {
        runOnUiThread {
            chargerListAdapter.addItems(chargersLinked)
        }
    }

    override fun showLoading() {
        binding.progressBar.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        runOnUiThread {
            binding.progressBar.visibility = View.GONE
        }
    }

    override fun checkPermissions() {
        checkWallboxSDKPermissions(
            onPermissionNotGranted = {
                ActivityCompat.requestPermissions(this,
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.BLUETOOTH,
                        Manifest.permission.BLUETOOTH_ADMIN), REQUEST_LOCATION)
            },
            onPermissionGranted = {
                presenter.onPermissionsGranted()
            }
        )
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            REQUEST_LOCATION -> {
                if (grantResults.isNotEmpty() && grantResults[0]
                    == PackageManager.PERMISSION_GRANTED
                    && ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED
                ) {
                    presenter.onPermissionsGranted()
                } else {
                    presenter.loadChargers()
                }
                return
            }
        }
    }

    override fun checkBluetoothIsActive() {
        if (!bluetoothAdapter.isEnabled) {
            startActivityForResult(
                Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE),
                REQUEST_ENABLE_BT
            )
        } else {
            presenter.scanChargers()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.menu_charger_list, menu)
        return true
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.add -> navigateToAddCharger()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun showAddedChargerSuccessfully() {
        binding.cdlRoot.showSnackBarSuccessful(resources.getString(R.string.added_charger))
    }

    override fun cleanAdapter() {
        chargerListAdapter.clean()
    }

    private fun navigateToChargerDetail(serialNumber: Long) {
        startActivityForResult(ChargerDetailActivity.create(this, serialNumber), REQUEST_CONFIG_CHARGER)
    }

    private fun setUpRecyclerView() {
        binding.rcvChargerList.apply {
            layoutManager = LinearLayoutManager(this.context, LinearLayoutManager.VERTICAL, false)
            adapter = chargerListAdapter
            val itemDecoration = DividerItemDecoration(context, LinearLayoutManager.VERTICAL)
            getDrawable(R.drawable.divider)?.let {
                itemDecoration.setDrawable(it)
            }
            addItemDecoration(itemDecoration)
        }
    }

    private fun setUpListeners() {
        binding.root.swpChargerList.setOnRefreshListener {
            presenter.reloadLinkedChargers()
            binding.root.swpChargerList.isRefreshing = false
        }
    }

    private fun navigateToAddCharger() {
        startActivityForResult(AddChargerActivity.create(this), REQUEST_ADD_CHARGER)
    }
}