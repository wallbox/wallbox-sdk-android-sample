package com.example.wallboxsdksample.chargerList

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.wallboxsdksample.R
import com.example.wallboxsdksample.databinding.ItemChargerBinding
import com.example.wallboxsdksample.loadUrl
import com.example.wallboxsdksample.model.Charger
import com.example.wallboxsdksample.model.ChargerStatus

class ChargerListAdapter(
    private val itemClick: (Long) -> Unit
) : RecyclerView.Adapter<ChargerListAdapter.ItemHolder>() {

    private val chargersLinked: MutableList<Charger> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemHolder =
        ItemHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_charger, parent, false)
        )

    override fun onBindViewHolder(holder: ItemHolder, position: Int) =
        holder.configure(chargersLinked[position], itemClick)

    override fun getItemCount(): Int = chargersLinked.size

    fun addItems(chargersLinked: List<Charger>) {
        this.chargersLinked.addAll(chargersLinked)
        notifyDataSetChanged()
    }

    fun clean() {
        chargersLinked.clear()
        notifyDataSetChanged()
    }

    class ItemHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val binding = ItemChargerBinding.bind(itemView)

        fun configure(
            charger: Charger,
            itemClick: (Long) -> Unit
        ) {
            binding.apply {
                lblNameCharger.text = charger.name
                imvCharger.loadUrl(charger.imageUrl ?: "")
                lblAvailabilityCharger.text = getStatus(charger)
                cnlCharger.setOnClickListener { itemClick(charger.serialNumber) }
            }
        }

        private fun getStatus(charger: Charger) =
            when (charger.status) {
                ChargerStatus.BluetoothAndInternetConnection ->
                    itemView.context.resources.getString(
                        R.string.bt_and_internet_connection,
                        charger.internetConnectionType ?: ""
                    )
                ChargerStatus.BluetoothConnection ->
                    itemView.context.resources.getString(R.string.bt_connection)
                ChargerStatus.InternetConnection ->
                    itemView.context.resources.getString(
                        R.string.internet_connection,
                        charger.internetConnectionType ?: ""
                    )
                else -> itemView.context.resources.getString(R.string.not_connection)
            }

    }

}