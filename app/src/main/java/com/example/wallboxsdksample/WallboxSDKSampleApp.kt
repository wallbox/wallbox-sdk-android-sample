package com.example.wallboxsdksample

import android.app.Application
import androidx.appcompat.app.AppCompatDelegate
import com.wallbox.sdk.WallboxSdk

class WallboxSDKSampleApp: Application() {

    override fun onCreate() {
        super.onCreate()
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        WallboxSdk.setup(application = this, testMode = true)
    }
}